﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveByMouse : MonoBehaviour {

    public Transform target; //represents the object you want to push about.
    public float speed = 1; //movement speed?
    public bool startMoving; //use this to flag when to move
    Vector3 direction;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(startMoving)
        {
            target.position += direction;
        }

	}

    public void _moveVertical(bool invertDirection)
    {
        startMoving = true;
        direction = (!invertDirection) ? target.up : -target.up; //is checking if invert direction is on
        direction = direction * speed;
    }

    public void _moveHorizontal(bool invertDirection)
    {
        startMoving = true;
        direction = (!invertDirection) ? target.right : -target.right; //is 
        direction = direction * speed;
    }

    public void StopMoving()
    {
        startMoving = false;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraPan : MonoBehaviour {

	[SerializeField]
	Camera _camera;

    [SerializeField]
    GameObject target;

    public int Boundary = 50;
    public float speed = 1.0F;

    public Text mouseXoutput;
    public Text mouseYoutput;

    [SerializeField]
    int _screenWidth;
    [SerializeField]
    int _screenHeight;

    [SerializeField] double mapX = 100.0;
    [SerializeField] double mapY = 100.0;
    public float minX, maxX, maxY, minY;

    // Use this for initialization
    void Start () {
        //get the camera reference for frustrum manip

        var vertExtent = Camera.main.orthographicSize;
        var horzExtent = vertExtent * Screen.width / Screen.height;
        _screenHeight = Screen.height;
        _screenWidth = Screen.width;
        minX = (float)(horzExtent - mapX / 2.0);
        maxY = (float)( mapY / 2.0 - vertExtent);
        maxX = (float)( mapX / 2.0 - horzExtent);
        minY = (float)( vertExtent - mapY / 2.0);

        _camera = gameObject.GetComponent<Camera>();
	}

    // Update is called once per frame
    void Update() {

    }

    void FixedUpdate()
    {
        //float horizontal = Input.GetAxis("Horizontal") * speed;
        //float vertical = Input.GetAxis("Vertical") * speed;
        //transform.Translate(horizontal, vertical, 0);
        //if (Input.mousePosition.x > _screenWidth - Boundary)
        //{
        //    //transform.position.x += speed * Time.deltaTime; // move on +X axis
        //    transform.Translate(speed*Time.deltaTime, 0, 0);

        //}
        //if (Input.mousePosition.x < 0 + Boundary)
        //{
        //    //transform.position.x -= speed * Time.deltaTime; // move on -X axis
        //    transform.Translate(-speed * Time.deltaTime, 0, 0);
        //}
        //if (Input.mousePosition.y > _screenHeight - Boundary)
        //{
        //    //transform.position.y += speed * Time.deltaTime; // move on +Z axis
        //    transform.Translate(0,speed * Time.deltaTime, 0);
        //}
        //if (Input.mousePosition.y < 0 + Boundary)
        //{
        //    //transform.position.y -= speed * Time.deltaTime; // move on -Z axis
        //    transform.Translate(0, -speed * Time.deltaTime, 0);
        //}


        mouseXoutput.text = Input.mousePosition.x.ToString();
        mouseYoutput.text = Input.mousePosition.y.ToString();
    }

    //GameObject Movement

    void OnGui()
    {
        GUI.Box(new Rect((Screen.width / 2) - 140, 5, 280, 25), "Mouse Position = " + Input.mousePosition);
        GUI.Box( new Rect((Screen.width / 2) - 70, Screen.height - 30, 140, 25), "Mouse X = " + Input.mousePosition.x);
        GUI.Box(new Rect(5, (Screen.height / 2) - 12, 140, 25), "Mouse Y = " + Input.mousePosition.y);
    }
}
